import BaseApi from "./BaseApi";
import TaskList from "./../domain/TaskList";

export default class TaskListApi extends BaseApi {
    static createList(taskList: TaskList): Promise<Response> {
        let url: string = this.API_URL + "/list/new";
        return fetch(url, {
            method: "POST",
            headers: new Headers({"content-type": "application/json"}),
            body: JSON.stringify(taskList).split("_").join(""),
        });
    }

    static getAllLists(): Promise<Response> {
        let url: string = this.API_URL + "/list/all";
        return fetch(url, {
            method: "GET",
        });
    }

    static deleteList(id: number): Promise<Response> {
        let url: string = this.API_URL + "/list/" + id;
        return fetch(url, {
            method: "DELETE",
        });
    }

    static completeList(taskList: TaskList): Promise<Response> {
        let url: string = this.API_URL + "/list/" + taskList.id;
        return fetch(url, {
            method: "PUT",
            headers: new Headers({"content-type": "application/json"}),
            body: JSON.stringify(taskList).split("_").join(""),
        });
    }
}