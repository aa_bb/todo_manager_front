import BaseApi from "./BaseApi";
import Task from "../domain/Task";

export default class TaskApi extends BaseApi {
    static addNewTask(task: Task, list_id: number): Promise<Response> {
        let url = this.API_URL + "/" + list_id + "/task/add";
        return fetch(url, {
            method: "POST",
            headers: new Headers({"content-type": "application/json"}),
            body: JSON.stringify(task).split("_").join(""),
        });
    }

    static getAllTasks(list_id: number): Promise<Response> {
        let url = this.API_URL + "/task/all/" + list_id;
        return fetch(url, {
            method: "GET",
            }
        );
    }

    static deleteTask(id: number): Promise<Response> {
        let url = this.API_URL + "/task/" + id;
        return fetch(url, {
            method: "DELETE",
            }
        );
    }

    static completeTask(task: Task): Promise<Response> {
        let url = this.API_URL + "/task/" + task.id;
        return fetch(url, {
            method: "PUT",
            headers: new Headers({"content-type": "application/json"}),
            body: JSON.stringify(task).split("_").join(""),
        });
    }
}