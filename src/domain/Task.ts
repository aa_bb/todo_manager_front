export default class Task {
   private _id: number;
   private _description: string;
   private _duration: number;
   private _isDone: boolean;
   private _listId: number;

   constructor() {}

   static fromJson(value: object): Task {
       let task: Task = new Task();
       task.id = value["id"];
       task.description = value["description"];
       task.duration = value["duration"];
       task.isDone = value["isDone"];
       task.listId = value["listId"];
       return task;
   }

   get id(): number {
       return this._id;
   }

   set id(value: number) {
       this._id = value;
   }

   get description(): string {
       return this._description;
   }

   set description(value: string) {
       this._description = value;
   }

   get duration(): number {
       return this._duration;
   }

   set duration(value: number) {
       this._duration = value;
   }

   get isDone(): boolean {
       return this._isDone;
   }

   set isDone(value: boolean) {
       this._isDone = value;
   }

   get listId(): number {
       return this._listId;
   }

   set listId(value: number) {
       this._listId = value;
   }
}