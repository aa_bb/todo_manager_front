export default class TaskList {
    private _id: number;
    private _listName: string;

    constructor() {}

    static fromJson(value: object): TaskList {
        let task_list: TaskList = new TaskList();
        task_list.id = value["id"];
        task_list.list_name = value["listName"];
        return task_list;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get list_name(): string {
       return this._listName;
    }

    set list_name(value: string) {
        this._listName = value;
    }
}