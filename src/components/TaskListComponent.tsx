import * as React from "react";
import TaskList from "./../domain/TaskList";
import Task from "../domain/Task";
import TaskComponent from "./TaskComponent";
import BaseButton from "./BaseButton";
import TaskApi from "../api/TaskApi";
import TaskListApi from "../api/TaskListApi";

export interface TaskListProps {
    taskList: TaskList;
    deleteList: any;
}

export interface TaskListState {
    tasksArray: Array<Task>;
    newTask: Task;
}

export default class TaskListComponent extends React.PureComponent<TaskListProps, TaskListState> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            tasksArray: [],
            newTask: new Task(),
        };
        this.deleteThisList = this.deleteThisList.bind(this);
        this.changeDuration = this.changeDuration.bind(this);
        this.changeDescription = this.changeDescription.bind(this);
        this.sendFormCreateNewTask = this.sendFormCreateNewTask.bind(this);
    }

    deleteTask = (value) => {
        this.setState({
            tasksArray: this.state.tasksArray.filter((task: Task) => {
                return task.id !== value;
            })
        });
    }

    deleteThisList(): void {
        TaskListApi.deleteList(this.props.taskList.id);
        this.props.deleteList(this.props.taskList.id);
    }

    updateTaskCompleteStatus = (task: Task, index) => {
        this.state.tasksArray[index] = task;
        this.forceUpdate();
    }

    changeDuration(event): void {
        this.state.newTask.duration = parseFloat(event.target.value);
    }

    changeDescription(event): void {
        this.state.newTask.description = event.target.value;
    }

    sendFormCreateNewTask(): void {
        TaskApi.addNewTask(this.state.newTask, this.props.taskList.id).then((response: Response) => {
            response.json().then((value: Task) => {
                this.setState({newTask: Task.fromJson(value)});
                this.setState({tasksArray: this.state.tasksArray.concat(this.state.newTask)});
            });
        });
    }

    componentDidMount(): void {
        TaskApi.getAllTasks(this.props.taskList.id).then((response: Response) => {
            response.json().then((value: Array<TaskList>) => {
                let tasks: Array<Task> = value.map((obj: object) =>
                    Task.fromJson(obj));
                this.setState({tasksArray: tasks});
            });
        });
    }

    render () {
        let createNewTaskActionsArray: Array<any> = [this.changeDuration, this.changeDescription, this.sendFormCreateNewTask];
        let deleteListActionsArray: Array<any> = [this.deleteThisList];
        return (
            <ul className={"taskList"}>
                TaskList{this.props.taskList.id}&emsp;{this.props.taskList.list_name}
                <BaseButton
                    buttonClass={"smallButton"}
                    portalClass={"task portal"}
                    buttonText={"Create New Task"}
                    mainTag={"span"}
                    buttonType={"createNewTask"}
                    actionsArray={createNewTaskActionsArray}
                />
                <BaseButton
                    buttonClass={"smallButton"}
                    portalClass={"taskList portal"}
                    buttonText={"Delete List"}
                    mainTag={"span"}
                    buttonType={"areYouSureList"}
                    actionsArray={deleteListActionsArray}
                />
                {
                    this.state.tasksArray.map((currentTask: Task, index: number) =>
                        <TaskComponent
                            key={currentTask.id}
                            task={currentTask}
                            index={index}
                            deleteTask={this.deleteTask}
                            updateTaskCompleteStatus={this.updateTaskCompleteStatus}
                        />
                    )
                }
            </ul>
        );
    }
}