import * as React from "react";
import * as ReactDOM from "react-dom";

export interface PortalProps {
    portalClass: string;
    portalType: string;
    portalText: string;

    togglePortalActionsArray: Array<any>;
    actionsArray: Array<any>;
}

export interface PortalState {
}

export default class Portal extends React.Component<PortalProps, PortalState> {
    root: any;

    componentWillMount(): void {
        this.root = document.createElement("div");
        document.body.appendChild(this.root);
    }

    componentDidMount(): void {
        this.props.togglePortalActionsArray[1]();
    }

    componentWillUnmount(): void {
        this.props.togglePortalActionsArray[1]();
        document.body.removeChild(this.root);
    }

    addTogglePortalToFunc(func: Function): void {
        func();
        this.props.togglePortalActionsArray[0]();
    }

    createPortalContent(): JSX.Element {
        if (this.props.portalType === "createNewList")
            return React.createElement(
                "div",
                {className: this.props.portalClass},
                <img
                    className={"cross"}
                    src={"../../src/assets/img/cross.gif"}
                    onClick={this.props.togglePortalActionsArray[0]}
                />,
                <form>
                    Name: &emsp;
                    <input type={"text"} onChange={this.props.actionsArray[0]} autoFocus={true}/>
                    &emsp;
                    <input
                        type={"button"}
                        value={"Save"}
                        onClick={() => this.addTogglePortalToFunc(this.props.actionsArray[1])}
                    />
                </form>
            );

        if (this.props.portalType === "createNewTask")
            return React.createElement(
                "div",
                {className: this.props.portalClass},
                <img
                    className={"cross"}
                    src={"../../src/assets/img/cross.gif"}
                    onClick={this.props.togglePortalActionsArray[0]}
                />,
                <form>
                    Duration: &emsp;
                    <input
                        type={"text"}
                        onChange={this.props.actionsArray[0]}
                        autoFocus={true}
                    /> <br/> <br/>
                    Description <br/>
                    <textarea cols={50} rows={5} onChange={this.props.actionsArray[1]}/>
                    <br/>
                    <input
                        className={"smallButton"}
                        type={"button"}
                        value={"Send"}
                        onClick={() => this.addTogglePortalToFunc(this.props.actionsArray[2])}
                    />
                </form>
            );

        if (this.props.portalType === "areYouSureTask" || this.props.portalType === "areYouSureList")
            return React.createElement(
                "div",
                {className: this.props.portalClass},
                this.props.portalText,
                <br/>,
                <form>
                    <input
                        className={"smallButton"}
                        type={"button"}
                        value={"Yes"}
                        onClick={() => this.addTogglePortalToFunc(this.props.actionsArray[0])}
                    />
                    &emsp;
                    <input
                        className={"smallButton"}
                        type={"button"}
                        value={"No"}
                        onClick={this.props.togglePortalActionsArray[0]}
                        autoFocus={true}
                    />
                </form>
            );
        return null;
    }

    render() {
        return ReactDOM.createPortal(this.createPortalContent(), this.root);
    }
}