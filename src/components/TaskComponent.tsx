import * as React from "react";
import Task from "../domain/Task";
import BaseButton from "./BaseButton";
import TaskApi from "../api/TaskApi";

export interface TaskProps {
    task: Task;
    index: number;
    deleteTask: any;
    updateTaskCompleteStatus: any;
}

export interface TaskState {}

export default class TaskComponent extends React.PureComponent<TaskProps, TaskState> {
    constructor(props, context) {
        super(props, context);
        this.deleteThisTask = this.deleteThisTask.bind(this);
        this.toggleTaskCompleteStatus = this.toggleTaskCompleteStatus.bind(this);
    }

    deleteThisTask(): void {
        TaskApi.deleteTask(this.props.task.id);
        this.props.deleteTask(this.props.task.id);
    }

    toggleTaskCompleteStatus(): void {
        TaskApi.completeTask(this.props.task).then((response: Response) => {
            response.json().then((value: Task) => {
                this.props.updateTaskCompleteStatus(Task.fromJson(value), this.props.index);
            });
        });
    }

    render() {
        let deleteTaskActionsArray: Array<any> = [this.deleteThisTask];
        let toggleFinishActionsArray: Array<any> = [this.toggleTaskCompleteStatus];
        let message: string = this.props.task.isDone ? "Set Undone" : "I Finish It!";
        let addClass: string = this.props.task.isDone ? " complete" : "";
        return (
            <li className={"task" + addClass}>
                Task{ this.props.task.id } &emsp;
                { this.props.task.duration.toFixed(2)/* знаки после точки*/}
                <BaseButton
                    buttonClass={"smallButton"}
                    portalClass={""}
                    buttonText={message}
                    mainTag={"span"}
                    buttonType={"finish"}
                    actionsArray={toggleFinishActionsArray}
                />
                <BaseButton
                    buttonClass={"delTaskButton"}
                    portalClass={"task portal"}
                    buttonText={""}
                    mainTag={"div"}
                    buttonType={"areYouSureTask"}
                    actionsArray={deleteTaskActionsArray}
                />
                <p className={"text"}> {this.props.task.description} </p>
            </li>
        );
    }
}