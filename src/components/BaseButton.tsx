import * as React from "react";
import Portal from "./Portal";
import {PortalContext} from "./PortalContext";

export interface BaseButtonProps {
    buttonClass: string;
    portalClass: string;
    buttonText: string;
    mainTag: string;
    buttonType: string;
    actionsArray: Array<any>;
}

export interface BaseButtonState {
    isPortalOpen: boolean;
}

export default class BaseButton extends React.Component <BaseButtonProps, BaseButtonState> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            isPortalOpen: false,
        };
        this.openPortal = this.openPortal.bind(this);
        this.closePortal = this.closePortal.bind(this);
    }

    openPortal(isAnyPortalOpen: boolean): void {
        if (!isAnyPortalOpen)
        this.setState({isPortalOpen: true});
    }

    closePortal(): void {
        this.setState({isPortalOpen: false});
    }

    chooseButtonContent(isAnyPortalOpen: boolean): JSX.Element {
        if (this.props.buttonType === "finish")
            return React.createElement(
                this.props.mainTag,
                {className: this.props.buttonClass, onClick: this.props.actionsArray[0]},
                this.props.buttonText
            );
        return React.createElement(
            this.props.mainTag,
            {className: this.props.buttonClass, onClick: () => this.openPortal(isAnyPortalOpen)},
            this.props.buttonText
        );
    }

    chosePortalContent(isAnyPortalOpen: boolean, toggleGlobalPortalState: any): JSX.Element {
        let togglePortalActionsArray: Array<any> = [this.closePortal, toggleGlobalPortalState];
        if (this.props.buttonType === "createNewList")
            return React.createElement(
                this.props.mainTag,
                {},
                this.state.isPortalOpen &&
                <Portal
                    portalClass={this.props.portalClass}
                    portalType={this.props.buttonType}
                    portalText={""}
                    togglePortalActionsArray={togglePortalActionsArray}
                    actionsArray={this.props.actionsArray}
                />
            );
        if (this.props.buttonType === "createNewTask")
            return React.createElement(
                this.props.mainTag,
                {},
                this.state.isPortalOpen &&
                <Portal
                    portalClass={this.props.portalClass}
                    portalType={this.props.buttonType}
                    portalText={""}
                    togglePortalActionsArray={togglePortalActionsArray}
                    actionsArray={this.props.actionsArray}
                />
            );
        if (this.props.buttonType === "areYouSureTask")
            return React.createElement(
                this.props.mainTag,
                {},
                this.state.isPortalOpen &&
                <Portal
                    portalClass={this.props.portalClass}
                    portalType={this.props.buttonType}
                    portalText={"Are you sure you want to delete this task?"}
                    togglePortalActionsArray={togglePortalActionsArray}
                    actionsArray={this.props.actionsArray}
                />
            );
        if (this.props.buttonType === "areYouSureList")
            return React.createElement(
                this.props.mainTag,
                {},
                this.state.isPortalOpen &&
                <Portal
                    portalClass={this.props.portalClass}
                    portalType={this.props.buttonType}
                    portalText={"Are you sure you want to delete this list?"}
                    togglePortalActionsArray={togglePortalActionsArray}
                    actionsArray={this.props.actionsArray}
                />
            );
        return null;
    }

    render() {
        return (
            <PortalContext.Consumer>
                {({isAnyPortalOpen, toggleGlobalPortalState}) => (
                    React.createElement(
                        this.props.mainTag,
                        {},
                        this.chooseButtonContent(isAnyPortalOpen),
                        this.chosePortalContent(isAnyPortalOpen, toggleGlobalPortalState)
                    ))}
            </PortalContext.Consumer>);
    }
}