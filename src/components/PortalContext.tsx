import * as React from "react";

export const PortalContext = React.createContext({
    isAnyPortalOpen: false,
    toggleGlobalPortalState: () => {},
});