import * as React from "react";
import "./../assets/scss/App.scss";
import TaskListComponent from "./TaskListComponent";
import BaseButton from "./BaseButton";
import TaskList from "../domain/TaskList";
import TaskListApi from "../api/TaskListApi";
import {PortalContext} from "./PortalContext";

export interface AppProp {}

export interface AppState {
    taskListsArray: Array<TaskList>;
    newTaskList: TaskList;
    isAnyPortalOpen: boolean;
    toggleGlobalPortalState: any;
}

export default class App extends React.Component<AppProp, AppState> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            taskListsArray: [],
            newTaskList: new TaskList(),
            isAnyPortalOpen: false,
            toggleGlobalPortalState: this.toggleGlobalPortalState.bind(this),
        };
        this.changeListName = this.changeListName.bind(this);
        this.sendFormCreateNewList = this.sendFormCreateNewList.bind(this);
        this.deleteList = this.deleteList.bind(this);
    }

    toggleGlobalPortalState(): void {
        this.setState({isAnyPortalOpen: !this.state.isAnyPortalOpen});
    }

    changeListName(event): void {
        this.state.newTaskList.list_name = event.target.value;
    }

    sendFormCreateNewList(): void {
        TaskListApi.createList(this.state.newTaskList).then((response: Response) => {
            response.json().then((value: TaskList) => {
                this.setState({newTaskList:  TaskList.fromJson(value)});
                this.setState({taskListsArray: this.state.taskListsArray.concat(this.state.newTaskList)});
            });
        });
        this.setState({newTaskList: new TaskList()});
    }

    deleteList = (value) => {
        this.setState({taskListsArray: this.state.taskListsArray.filter((taskList: TaskList) => {
            return taskList.id !== value;
            })
        });
    }

    componentDidMount(): void {
        TaskListApi.getAllLists().then((response: Response) => {
            response.json().then((value: Array<TaskList>) => {
                    let taskLists: Array<TaskList> = value.map((obj: object) =>
                        TaskList.fromJson(obj));
                    this.setState({taskListsArray: taskLists});
            });
        });
    }

    render() {
        let createNewListActionsArray: Array<any> = [this.changeListName, this.sendFormCreateNewList];
        return (
            <PortalContext.Provider value={this.state}>
            <div id={"app"}>
                <h1>ToDo List Manager</h1>
                <BaseButton
                    buttonClass={"createListButton"}
                    portalClass={"createListButton portal"}
                    buttonText={"Create New List"}
                    mainTag="div"
                    buttonType={"createNewList"}
                    actionsArray={createNewListActionsArray}
                />
                {
                    this.state.taskListsArray.map((currentTaskList: TaskList) =>
                        <TaskListComponent
                            key={currentTaskList.id}
                            taskList={currentTaskList}
                            deleteList={this.deleteList}
                        />
                    )
                }
             </div>
            </PortalContext.Provider>
        );
    }
}
